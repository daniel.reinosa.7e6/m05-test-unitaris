/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.4 Calcula l’àrea
*/

import java.util.*
//  Reads an int from the input and prints it.
fun main() {
    // Prepare the Scanner
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix l'amplada en metres:")
    // Reads an int
    val userInputValue = scanner.nextDouble()

    println("Introdueix la llargada en metres:")
    // Reads an int
    val userInputValue2 = scanner.nextDouble()
    println("Metres cuadrats de l'habitatge: ${area(userInputValue, userInputValue2)}")


}
fun area (userInputValue: Double, userInputValue2: Double):Double{
    if(userInputValue > 0.0 && userInputValue2 > 0.0)return userInputValue * userInputValue2
    else return 0.0

}
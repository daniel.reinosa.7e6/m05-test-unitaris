/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.11 Calculadora de volum d’aire
*/

import java.util.*
import java.util.function.DoublePredicate


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Indiqueu la mida del aula X Y Z")

    val x = scanner.nextDouble()
    val y = scanner.nextDouble()
    val z = scanner.nextDouble()
    println("El volum al aula es de : ${midaAula(x, y, z)}")

}
fun midaAula(x:Double, y:Double, z:Double):Double{
    if (x < 0 || y < 0 || z < 0)return 0.0
    else return (x * y * z)
}
/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.14 Divisor de compte
*/

import java.util.*


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Indica el numero de persones i el preu final del sopar")


    val preu = scanner.nextDouble()
    val pers = scanner.nextDouble()

    println("Preu final per persona:${divisor(preu, pers)}")

    println("€")
}
fun divisor(preu: Double, pers: Double):Double{
    return preu/pers

}
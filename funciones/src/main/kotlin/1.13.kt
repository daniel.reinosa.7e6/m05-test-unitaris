/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.13 Quina temperatura fa?
*/

import java.util.*
import javax.print.attribute.standard.DocumentName


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Quina temperatura fa? Quant ha agumentat?")

    val temp = scanner.nextDouble()
    val aug = scanner.nextDouble()

    println("La temperatura actual es de: ${temperatura(temp, aug)}")
    println("º")
}
fun temperatura(temp: Double, aug: Double):Double{
    return temp+aug
}
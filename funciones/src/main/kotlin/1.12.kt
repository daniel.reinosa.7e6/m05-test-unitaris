/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.12 De Celsius a Fahrenheit
*/

import java.nio.DoubleBuffer
import java.util.*


fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Indica la temperatura en Celsius per convertir-la en graus Fahrenheit:")

    val celsius = scanner.nextDouble()

    print("Graus: ${conversion(celsius)} ")

    println("°F\n")
}
fun conversion (celsius: Double):Double{
    return (celsius * 9 / 5) + 32
}

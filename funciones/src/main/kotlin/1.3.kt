/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.2 Dobla l’enter
*/

import java.util.*
//  Reads an int from the input and prints it.
fun main() {
    // Prepare the Scanner
    val scanner = Scanner(System.`in`)
    println("Introdueix un número:")
    // Reads an int
    val userInputValue = scanner.nextInt()

    println("Aquest és el número introduït: ${multy(userInputValue)}")

}
fun multy (userInputValue: Int):Int{
   return userInputValue*2

}
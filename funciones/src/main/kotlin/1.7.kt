/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.7 Número següent
*/
import java.util.*
//  Reads an int from the input and prints it.
fun main() {
    // Prepare the Scanner
    val scanner = Scanner(System.`in`)

    println("Introdueix un numero enter")

    val userInputValue = scanner.nextInt()

    println("El resultat: ${nextnum(userInputValue)}")

}
fun nextnum (userInputValue: Int):Int{
     if (userInputValue < 0)return userInputValue -1
     else return userInputValue + 1
}
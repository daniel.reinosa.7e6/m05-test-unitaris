/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.10 Quina és la mida de la meva pizza?
*/

import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica el diametre de la pizza")


    var NumPI = Math.PI
    var Diametre  = scanner.nextDouble()

    println("Area de la pizza es:${mida(Diametre)}")

}

fun mida(Diametre: Double):Double{
    if (Diametre <0) return 0.0
    else return Math.PI * Diametre/2 * Diametre/2

}
/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.8 Dobla el decimal
*/

import java.util.*
//  Reads an int from the input and prints it.
fun main() {
    // Prepare the Scanner
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix un numero ")
    val numero = scanner.nextDouble()

    println("El doble es: ${multiply(numero)}")


}
fun multiply (numero: Double):Double{
    return numero * 2
}
/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.9 Calcula el descompte
*/

import java.util.*
fun main() {
    val scanner = Scanner(System.`in`).useLocale(Locale.UK)
    println("Introdueix preu final sense descompte ")
    val scanner1 = scanner.nextDouble()
    println("Introdueix preu final amb descompte ")
    val scanner2 = scanner.nextDouble()
    println("El descompte es: ${descomte(scanner1,scanner2)} ")

}
fun descomte(scanner1: Double, scanner2:Double):Double{
    if (scanner1 == 0.0|| scanner1 <0) return  0.0
    else return (100 - (scanner2*100/scanner1))
}


/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.16 Transforma l’enter
*/

import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica un numero enter:")

    val trans = scanner.nextInt()

    println("El numero en decimal es:${change(trans)}")

}
fun change (trans : Int):Double{

    return trans.toDouble()
}
/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.15 Afegeix un segon
*/

import java.util.*


fun main() {
    val scanner = Scanner(System.`in`)
    println("Indica els segons:")

    val seg = scanner.nextInt()

    println("Els minuts son:${second(seg)}")

}
fun second (seg: Int):Int{
    if (seg < 0 || seg ==0)return 0
    else return (seg + 1) %60
}


/*
* AUTHOR: Daniel Reinosa Luque
* DATE: 2022/09/19
* TITLE: 1.3 Suma de dos nombres enters
*/
import java.util.*
//  Reads an int from the input and prints it.
fun main() {
    // Prepare the Scanner
    val scanner = Scanner(System.`in`)
    println("Introdueix dos números per a sumar:")
    // Reads an int
    val userInputValue = scanner.nextInt()
    // Reads an int
    val userInputValue2 = scanner.nextInt()

    println("Aquesta és la suma de números introduïts: ${sum(userInputValue, userInputValue2)}")

}
fun sum (userInputValue: Int, userInputValue2:Int):Int{

    return userInputValue + userInputValue2

}

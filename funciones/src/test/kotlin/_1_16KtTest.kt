import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_16KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(32.0, change(32))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(-2.0 , change(-2))
    }
    @Test
    fun whenIsLong (){
        assertEquals(932.0, change(932))
    }
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_6KtTest{
    @Test
    fun WhenValueIsDouble (){
        assertEquals(0, pupies(45, 0, 45))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(0, pupies(-432432, -4324234, -342))
    }
    @Test
    fun whenIsLong (){
        assertEquals(181606, pupies(324324, 4353, 34534 ))
    }

}
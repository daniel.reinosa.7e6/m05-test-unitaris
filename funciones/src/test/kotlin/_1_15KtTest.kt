import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_15KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(0, second(0))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(0, second(-2))
    }
    @Test
    fun whenIsLong (){
        assertEquals(15, second(543554))
    }
}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_12KtTest{

    @Test
    fun WhenValueZero (){
        assertEquals(32.0, conversion(0.0))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(28.4 , conversion(-2.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(932.0, conversion(500.0))
    }
}
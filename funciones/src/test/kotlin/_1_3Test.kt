import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_3Test{
    @Test
    fun multyZero (){
        assertEquals(0, multy(0))
    }
    @Test
    fun multyNegative (){
        assertEquals(-2, multy(-1))
    }
    @Test
    fun multylong (){
        assertEquals(8730908, multy(4365454))
    }


}


import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_13KtTest{
    @Test
    fun WhentempAug (){
        assertEquals(80.0, temperatura(30.0, 50.0))
    }
    @Test
    fun whenTempBaixa(){
        assertEquals(18.0 , temperatura(20.0,-2.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(932.0, temperatura(900.0, 32.0))
    }
}
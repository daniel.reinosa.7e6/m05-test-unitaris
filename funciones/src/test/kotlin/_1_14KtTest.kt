import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_14KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(0.0, divisor(0.0, 10.0))
    }
    @Test
    fun morePeopleThanPrice(){
        assertEquals(0.5 , divisor(10.0,20.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(166.66666666666666, divisor(500.0, 3.0))
    }
}
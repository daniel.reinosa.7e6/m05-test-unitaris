import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_4KtTest{
    @Test
    fun WhenValueIsZero (){
        assertEquals(0.0, area(0.0, 65545.0))
    }
    @Test
    fun whenIsNegativePrintZero (){
        assertEquals(0.0, area(-611.0, -4151.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(1939624935.0, area(44543.0, 43545.0))
    }

}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_8KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(0.0, multiply(0.0))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(-4.0, multiply(-2.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(10000.0, multiply(5000.0 ))
    }

}
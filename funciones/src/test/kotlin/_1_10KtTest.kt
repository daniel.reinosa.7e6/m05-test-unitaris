import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_10KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(0.0, mida(0.0))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(0.0, mida(-2.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(196349.54084936206, mida(500.0 ))
    }

}
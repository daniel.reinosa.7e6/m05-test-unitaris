import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_2KtTest{
    @Test
    fun sumInt (){
        assertEquals(4,sum(2,2))
    }

    @Test
    fun negativeInt(){
        assertEquals(-4,sum(-2,-2))
    }

    @Test
    fun longInt(){
        assertEquals(612148533, sum(555689568,56458965))
    }

}


import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_9KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(0.0, descomte(0.0, 45.0))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(0.0, descomte(-2.0, 56.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(99.5, descomte(6000.0, 30.0 ))
    }

}
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_5KtTest{
    @Test
    fun WhenValueIsZero (){
        assertEquals(0, operation(8000, 3000, 5000, 6000))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(-33296662, operation(-432432, -4324234, -342, -43))
    }
    @Test
    fun whenIsLong (){
        assertEquals(60816, operation(8364, 324, 2344, 334 ))
    }

}
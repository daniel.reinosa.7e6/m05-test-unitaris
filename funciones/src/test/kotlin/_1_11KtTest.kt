import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_11KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(0.0, midaAula(0.0, 45.0, 343.0))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(0.0, midaAula(-2.0, 50.0, 100.0))
    }
    @Test
    fun whenIsLong (){
        assertEquals(2.7E8, midaAula(500.0, 600.0, 900.0 ))
    }
}
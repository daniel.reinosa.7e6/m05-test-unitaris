import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class _1_7KtTest{
    @Test
    fun WhenValueZero (){
        assertEquals(1, nextnum(0))
    }
    @Test
    fun whenIsNegative(){
        assertEquals(-4, nextnum(-3))
    }
    @Test
    fun whenIsLong (){
        assertEquals(9999999, nextnum(9999998 ))
    }

}